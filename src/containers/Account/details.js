import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';
import fetch from 'cross-fetch';




export default class AntTable extends Component {


static defaultProps = {
    numberOfMonths: 2,
  };


  
 handleClick = () => {

  var thi = this;
   var filterCity = this.state.city;
   var filterEndDay = this.state.endDay;
   var filterStartDay = this.state.startDay;

  fetch('http://104.131.28.228/transporter/public/api/CitiesFilter', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({City:filterCity,endDay:filterEndDay,startDay:filterStartDay}) 


})
.then(function(data) {
  
  var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;



    thi.setState({
         data: coloum,
         columns : head,
         loading: false,
        
  });
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
   
    }







  constructor(props) {
   super(props);


   this.handleDayClick = this.handleDayClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);


   this.state = {
    loading: true,
    pagination : true,
    pick : this.getInitialState(),
     data :  [
        {
            "time": "2018-01-30",
            "delete" : "Delete",
            "friend": {
                "HisarusersCount": 2,
                "NOIDAusersCount": 1
            }
        }
    ],

 columns : [{
            "id": 0,
            "Header": "ID",
            "accessor": "uuid"
        },
        {
            "id": 0,
            "Header": "Date",
            "accessor": "created_at"
        },{
            "id": 0,
            "Header": "Name ",
            "accessor": "name"
        },{
            "id": 0,
            "Header": "Company Name ",
            "accessor": "company_name"
        },{
            "id": 0,
            "Header": "City ",
            "accessor": "registered_office_city"
        },{
            "id": 0,
            "Header": "Contact No.",
            "accessor": "phone_no"
        },
         {
            "id": 0,
            "Header": "Full Detail",
            "accessor": "detail"
        },{
            "id": 0,
            "Header": "Deactivate",
            "accessor": "delete"
        }
    ],
   

   }


   
 
 }

 startDate = (day) => {
this.setState({startDay : day });

}

 endDate = (day) => {
this.setState({endDay : day });

}


logChange = (val) => {
  this.setState({city : val});
}



getInitialState() {
    return {
      from: undefined,
      to: undefined,
    };
  }
  handleDayClick(day) {
    const range = DateUtils.addDayToRange(day, this.state);
    this.setState(range);
  }
  handleResetClick() {
    this.setState(this.getInitialState());
  }

 
 componentWillMount() {
 var th = this;

      var pageName = this.props.match.params.name;
      var pageType = this.props.match.params.type;
    
      var time = this.props.match.params.time;
      var city = this.props.match.params.city;

      var url ='http://104.131.28.228/transporter/public/api/users_data';
  if(pageName==='Deactivate' || pageName==='Approved')
  {
    var url = 'http://104.131.28.228/transporter/public/api/users_data_new'
  }


      fetch(url, {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({type:pageType,page:pageName,time:time,city:city}) 


})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.data;

  th.setState({
         data: coloum,
         loading: false,
   
  });

})
}


  render() {

   const { rowStyle, colStyle, gutter } = basicStyle;
  const { from, to } = this.state.pick;
    const modifiers = { start: from, end: to };
    var buttonAlign = {
      float:'right'
    }

    return (

<LayoutWrapper>
<Helmet>
          <style>{`
  .dYawgl {
    margin-top: 0px !important;}
    .ReactTable {
      margin-top:20px;
    }
    .rt-tr {
      font-size:16px;
    }
 .rt-tbody{
  text-align:center;
 }
 .rt-tr{
  cursor:pointer;
 }
`}</style>
        </Helmet>

        <PageHeader>Account Management</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
 



 <Col md={24} sm={24} xs={24} style={colStyle}>
   <Box>

       <PageHeader>{this.props.match.params.name} Signup</PageHeader>

      <ReactTable
       
      loadingText = 'Please Wait While Data Is Loading...'
      data={this.state.data}
      columns={this.state.columns}
      defaultPageSize={5}
      loading= {this.state.loading}
      showPagination={this.state.pagination}
      getTdProps={(state, rowInfo, column, instance) => {
    return {
      onClick: (e, handleOriginal) => {
        var city = column.Header;
        var time = rowInfo;
       
        var pageName = this.props.match.params.name;
        var pageType = this.props.match.params.type;
 
       var delid = time.original.id;
       
       if(city == 'Full Detail')
       {
          window.location.href = "http://localhost:3000/dashboard/User-profile/"+delid;
       }
       if(city=='Deactivate' && pageName!='Deactivate'){
       fetch('http://104.131.28.228/transporter/public/api/usersDelete', {
          method: 'post',
           headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({'id':delid}),

        })

         
         Notification(
                  'success',
                  'success',
                  'User Deactivated Successfully'
                );

                setTimeout(function () { window.location.reload() }, 200);

       }
if(city=='Deactivate' && pageName=='Deactivate'){
       fetch('http://104.131.28.228/transporter/public/api/usersreActivate', {
          method: 'post',
           headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({'id':delid}),

        })

         
         Notification(
                  'success',
                  'success',
                  'User Re Active Successfully'
                );

                setTimeout(function () { window.location.reload() }, 200);

       }

  //window.location.href = "http://localhost:3000/dashboard/"+pageType+"/"+pageName+"/"+time+"/"+city;

        if (handleOriginal) {
          handleOriginal()
        }
      }
    }
  }}
  />

        </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

