import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';

import fetch from 'cross-fetch';

const Option = SelectOption;
const { rowStyle, colStyle, gutter } = basicStyle;
  
    var buttonAlign = {
      float:'right'
    }
    const headingStyle = {
      top:'32px'
    }
export default class AntTable extends Component {



static defaultProps = {
    numberOfMonths: 2,
  };

 handleClick = () => {

  var thi = this;
   var filterCity = this.state.city;
   var filterEndDay = this.state.endDay;
   var filterStartDay = this.state.startDay;

  fetch('http://104.131.28.228/transporter/public/api/CitiesFilter', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({City:filterCity,endDay:filterEndDay,startDay:filterStartDay}) 


})
.then(function(data) {
  
  var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;



    thi.setState({
         data: coloum,
         columns : head,
         loading: false,
        
  });
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
   
    }







  constructor(props) {
   super(props);

    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);

   this.handleDayClick = this.handleDayClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);


   this.state = {
    city : 'All',
   	loading: true,
    other : '',
    pagination : true,
   	pick : this.getInitialState(),
         file: '',
      imagePreviewUrl: ''
   

   }


   
 
 }

_handleSubmit(e) {
    e.preventDefault();
    var img = this.state.imagePreviewUrl;

var image = img.substr(img.indexOf(',') + 1);



fetch('http://104.131.28.228/transporter/public/api/file', {
  method: 'post',
   headers: {'Accept': 'application/json'},
  body: JSON.stringify({side:'front',type:'id',image:image }), 

})
.then(function(data) {
  
  console.log(data);
 
}, function(error) {

  console.log(error);
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})

 


  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    console.log(file);
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }

 startDate = (day) => {
this.setState({startDay : day });

}

 otherData(event)  {
alert(event.target.value);

}


logChange = (val) => {

  if(val=='Other')
  {
    this.setState({other : <Col md={24} sm={24} xs={24} ><Col md={3} sm={3} xs={3} style={headingStyle}>  </Col><Col md={4} sm={4} xs={4} style={headingStyle}> Mention ID (type): </Col><Col md={14} sm={14} xs={14} style={colStyle}> <ContentHolder><input type="text" className="form-control" placeholder="please specify id type" name="other"    onChange={this.otherData}/> </ContentHolder></Col><Col md={4} sm={4} xs={4} style={headingStyle}> </Col></Col>});
  }
  else
  {

  }
}



getInitialState() {
    return {
      from: undefined,
      to: undefined,
    };
  }
  handleDayClick(day) {
    const range = DateUtils.addDayToRange(day, this.state);
    this.setState(range);
  }
  handleResetClick() {
    this.setState(this.getInitialState());
  }

 

  render() {

 let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {

      $imagePreview = (<img src={imagePreviewUrl} />);
    }

   
    return (


<LayoutWrapper>



        <PageHeader >Upload User's Document</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={headingStyle}>
            <Box>
 <Col md={24} sm={24} xs={24} >
<Col md={3} sm={3} xs={3} style={headingStyle}>  </Col>
<Col md={4} sm={4} xs={4} style={headingStyle}>  Selct ID (type): </Col>
<Col md={14} sm={14} xs={14} style={colStyle}>  
<ContentHolder>

<Helmet>
          <style>{`
  .dYawg {
    margin-top: 0px !important;
  }
   .filepicker{
    min-height:200px! important;

  }
  .portion{
    padding-top:25px !important;
  }
   img{
      max-height:240px !important;
      max-width:300px ! important;
    }
    .form-control1{
      padding:2px 10px !important;
      font-size: 13px !important ;
      display: inline-block;
      width: 80%;
    }
    .btn {
      font-size:13px !important;
    }

`}</style>
        </Helmet>

<Select
  style={{ width: '100%' }}
    placeholder="Please select"
      onChange={this.logChange}
                >
                 <Option key="Adhar Card">Adhar Card</Option>
                 <Option key="Driving Licence">Driving Licence</Option>
                 <Option key="Voter Id">Voter Id</Option>
                 <Option key="Passport">Passport</Option>
                 <Option key="Other">Other</Option>
                </Select>
              </ContentHolder>
              </Col>
<Col md={4} sm={4} xs={4} style={headingStyle}> </Col>
</Col>

{this.state.other}


 <Col md={24} sm={24} xs={24} className="portion" style={colStyle}> 
 <Col md={4} sm={4} xs={4} style={colStyle}> 
  Id Image Front:
 </Col>
 <Col md={20} sm={20} xs={20} style={colStyle}> 
<form onSubmit={this._handleSubmit}>
          <input type="file" className="form-control form-control1" onChange={this._handleImageChange} />
          <button type="submit" className="btn btn-info" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        <div className="img-responsive styl" >{$imagePreview}</div>

 </Col>
</Col>
<Col md={24} sm={24} xs={24} style={colStyle}>
<Col md={4} sm={4} xs={4} style={colStyle}> 
  Id Image Back:
 </Col>
 <Col md={20} sm={20} xs={20} style={colStyle}> 
<form onSubmit={this._handleSubmit}>
          <input type="file" className="form-control form-control1" onChange={this._handleImageChange} />
          <button type="submit" className="btn btn-info" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        <div className="img-responsive styl" >{$imagePreview}</div>
 </Col>
 </Col>


  <Col md={24} sm={24} xs={24} style={colStyle}> 
 <Col md={4} sm={4} xs={4} style={colStyle}> 
  Vehicle Front:
 </Col>
 <Col md={20} sm={20} xs={20} style={colStyle}> 
<form onSubmit={this._handleSubmit}>
          <input type="file" className="form-control form-control1" onChange={this._handleImageChange} />
          <button type="submit" className="btn btn-info" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        <div className="img-responsive styl" >{$imagePreview}</div>
 </Col>
 </Col>


  <Col md={24} sm={24} xs={24} style={colStyle}> 
<Col md={4} sm={4} xs={4} style={colStyle}> 
  Vehicle Back:
 </Col>
 <Col md={20} sm={20} xs={20} style={colStyle}> 
<form onSubmit={this._handleSubmit}>
          <input type="file" className="form-control form-control1" onChange={this._handleImageChange} />
          <button type="submit" className="btn btn-info" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        <div className="img-responsive styl" >{$imagePreview}</div>
 </Col>
 </Col>


 <Col md={24} sm={24} xs={24} style={colStyle}>  


<Button type="primary" style={buttonAlign} onClick={this.handleClick}>
            Filter
          </Button>
</Col>
 </Box>
 </Col>


        </Row>
      </LayoutWrapper>
    );
  }
}

