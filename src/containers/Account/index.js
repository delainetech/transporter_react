import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';
import fetch from 'cross-fetch';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';

import 'moment/locale/it';


const Option = SelectOption;

let children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}


export default class AntTable extends Component {


static defaultProps = {
    numberOfMonths: 2,
  };

 handleClick = () => {

  var thi = this;
   var filterCity = this.state.city;
   var filterEndDay = this.state.endDay;
   var filterStartDay = this.state.startDay;
    var pageName = this.props.match.params.name;
      var pageType = this.props.match.params.type;
var url ='http://104.131.28.228/transporter/public/api/CitiesFilter';
  if(pageName==='Deactivate' || pageName==='Approved')
  {
    var url = 'http://104.131.28.228/transporter/public/api/UserbyActionFilter'
  }
  fetch(url, {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({City:filterCity,endDay:filterEndDay,startDay:filterStartDay,type:pageType,page:pageName}) 


})
.then(function(data) {


  
  var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;



    thi.setState({
         data: coloum,
         columns : head,
         loading: false,
        
  });
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
   
    }







  constructor(props) {
   super(props);


   this.handleDayClick = this.handleDayClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);


   this.state = {
    city : 'All',
   	loading: true,
    optionValue : children,
    pagination : true,
   	pick : this.getInitialState(),
     data :  [
        {
            "time": "2018-01-30",
            "friend": {
                "HisarusersCount": 2,
                "NOIDAusersCount": 1
            }
        },
        {
            "time": "2018-01-31",
            "friend": {
                "HisarusersCount": 1,
                "NOIDAusersCount": 0
            }
        },
        {
            "time": "2018-02-01",
            "friend": {
                "HisarusersCount": 1,
                "NOIDAusersCount": 1
            }
        },
        {
            "time": "2018-02-02",
            "friend": {
                "HisarusersCount": 1,
                "NOIDAusersCount": 1
            }
        }
    ],

 columns : [
        {
            "id": 0,
            "registered_office_city": "",
            "Header": "Schedule date",
            "accessor": "time"
        },
        {
            "id": 1,
            "registered_office_city": "Hisar",
            "Header": "Hisar",
            "accessor": "d => d.friend.HisarusersCount"
        },
        {
            "id": 2,
            "registered_office_city": "NOIDA",
            "Header": "NOIDA",
            "accessor": "d => d.friend.NOIDAusersCount"
        }
    ],
   

   }


   
 
 }

 startDate = (day) => {
this.setState({startDay : day });

}

 endDate = (day) => {
this.setState({endDay : day });

}


logChange = (val) => {
  this.setState({city : val});
}



getInitialState() {
    return {
      from: undefined,
      to: undefined,
    };
  }
  handleDayClick(day) {
    const range = DateUtils.addDayToRange(day, this.state);
    this.setState(range);
  }
  handleResetClick() {
    this.setState(this.getInitialState());
  }

 
 componentWillMount() {
 var th = this;

      var pageName = this.props.match.params.name;
      var pageType = this.props.match.params.type;
  console.log(pageName);

  var url ='http://104.131.28.228/transporter/public/api/Cities';
  if(pageName==='Deactivate' || pageName==='Approved')
  {
    var url = 'http://104.131.28.228/transporter/public/api/UserbyAction'
  }
console.log(url);
      fetch(url, {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({type:pageType,page:pageName}) 


})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;
  let optionData = [];
 var optionTemplate = Select.map(v => (
  

      optionData.push(<Option key={v.City}>{v.City}</Option>)

    ));



  th.setState({
         data: coloum,
         columns : head,
         loading: false,
         optionValue : optionData
	});

})
}


  render() {

   const { rowStyle, colStyle, gutter } = basicStyle;
	const { from, to } = this.state.pick;
    const modifiers = { start: from, end: to };
    var buttonAlign = {
    	float:'right'
    }
    const headingStyle = {
      top:'2px'
    }
    return (

<LayoutWrapper>
        <PageHeader>Account Management</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={headingStyle}>
            <Box>

          
          <Col md={3} sm={3} xs={3} style={headingStyle}>  
          	Select Start Date:
          </Col>
          <Col md={4} sm={4} xs={4} style={colStyle}>  
        <DayPickerInput formatDate={formatDate}
        parseDate={parseDate}
        placeholder={`${formatDate(new Date())}`} format="LL" onDayChange={day =>this.startDate(day)} />
        </Col>

        <Col md={3} sm={3} xs={3} style={headingStyle}>  
          	Select End Date:
          </Col>
          <Col md={4} sm={4} xs={4} style={colStyle}>  
        <DayPickerInput formatDate={formatDate}
        parseDate={parseDate}
        placeholder={`${formatDate(new Date())}`}  format="LL" onDayChange={day => this.endDate(day)} />
        </Col>
<Col md={10} sm={10} xs={10} style={colStyle}>  
<Col md={4} sm={4} xs={4} style={headingStyle}>  
City:
</Col>


<Col md={20} sm={20} xs={20} style={colStyle}>  
<ContentHolder>

<Helmet>
          <style>{`
  .dYawgl {
    margin-top: 0px !important;}
    .ReactTable {
    	margin-top:20px;
    }
    .rt-tr {
    	font-size:16px;
    }
 .rt-tbody{
  text-align:center;
 }
 .rt-tr{
  cursor:pointer;
 }
 .ghxaZn{
   margin-top: 0px !important;
 }
 .isoExampleWrapper{
 	 margin-top: 0px !important;
 }
`}</style>
        </Helmet>


                <Select
            	
                  style={{ width: '100%' }}
                  placeholder="Please select"
            
                  onChange={this.logChange}
                >
                  {this.state.optionValue}
                </Select>
              </ContentHolder>
              </Col>
 </Col>

 <Col md={24} sm={24} xs={24} style={colStyle}>  

<Button type="primary" style={buttonAlign} onClick={this.handleClick}>
            Filter
          </Button>
</Col>
 </Box>
 </Col>



 <Col md={24} sm={24} xs={24} style={colStyle}>
   <Box>

       <PageHeader>{this.props.match.params.name} Signup</PageHeader>

      <ReactTable
       

    data={this.state.data}
    columns={this.state.columns}
   defaultPageSize={5}
   loading= {this.state.loading}
   showPagination={this.state.pagination}
  getTdProps={(state, rowInfo, column, instance) => {
    return {
      onClick: (e, handleOriginal) => {
        var city = column.Header;
        var time = rowInfo.original.time;
       
        var pageName = this.props.match.params.name;
        var pageType = this.props.match.params.type;
      
       if(city=='Schedule date'){
        city = 'All'
       }

  window.location.href = "http://localhost:3000/dashboard/"+pageType+"/"+pageName+"/"+time+"/"+city;

        if (handleOriginal) {
          handleOriginal()
        }
      }
    }
  }}
  />

        </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

