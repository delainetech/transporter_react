import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';
import fetch from 'cross-fetch';




export default class AntTable extends Component {


static defaultProps = {
    numberOfMonths: 2,
  };


 
constructor(props) {
   super(props);

this.state = {
    loading: true,
    pagination : true,
     data :  [
         {
            "id": 5,
            "name": "pankaj",
            "email": "pankajdelaine12@gmail.com",
            "password": "$2y$10$CZuZ/acxj31pz6AwYAzv9OC1EnPPVE8UQIPexCyeKs8GvYD0JSLlC",
            "role": 1,
            "phone": "8059170077",
            "remember_token": null,
            "updated_at": "2018-02-23 09:47:03",
            "pageNames": " Account Loads Referrals Reports Sub-Admins Truck Rating"
        }
    ],

 columns : [{
            "id": 0,
            "Header": "ID",
            "accessor": "id"
        },
        {
            "id": 0,
            "Header": "Name",
            "accessor": "name"
        },{
            "id": 0,
            "Header": "Email",
            "accessor": "name"
        },{
            "id": 0,
            "Header": "Phone No",
            "accessor": "phone"
        },{
            "id": 0,
            "Header": "Allowed Pages",
            "accessor": "pageNames",
             minWidth: 500,
        },{
            "id": 0,
            "Header": "Edit",
            "accessor": "edit",
             minWidth: 80,
        },{
            "id": 0,
            "Header": "Deactivate",
            "accessor": "Deactivate"
        }
    ],
   

   }


   
 
 }






 
 componentWillMount() {
 var th = this;


      var url ='http://104.131.28.228/transporter/public/api/sub_admin';

      fetch(url)
.then(function(data) {

console.log(data);
 var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.data;

  th.setState({
         data: coloum,
         loading: false,
   
  });

})
}


  render() {

   const { rowStyle, colStyle, gutter } = basicStyle;
 
    var buttonAlign = {
      float:'right'
    }

    return (

<LayoutWrapper>
<Helmet>
          <style>{`
  .dYawgl {
    margin-top: 0px !important;}
    .ReactTable {
      margin-top:20px;
    }
    .rt-tr {
      font-size:16px;
    }
 .rt-tbody{
  text-align:center;
 }
 .rt-tr{
  cursor:pointer;
 }
`}</style>
        </Helmet>

        <PageHeader>Sub Admins</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
 



 <Col md={24} sm={24} xs={24} style={colStyle}>
   <Box>

       <PageHeader>{this.props.match.params.name} Details</PageHeader>

      <ReactTable
       
      loadingText = 'Please Wait While Data Is Loading...'
      data={this.state.data}
      columns={this.state.columns}
      defaultPageSize={5}
      loading= {this.state.loading}
      showPagination={this.state.pagination}
      getTdProps={(state, rowInfo, column, instance) => {
    return {
      onClick: (e, handleOriginal) => {
        var city = column.Header;
        var time = rowInfo;

     
 
       var delid = time.original.id;
if(city=='Edit'){

if(process.env.NODE_ENV !== "production")
{
  window.location.href = "http://localhost:3000/dashboard/AdminEdit/"+delid;
}
else
{

  window.location.href = "http://104.131.28.228:9000/dashboard/AdminEdit/"+delid;
}
}
if(city=='Deactivate'){
       fetch('http://104.131.28.228/transporter/public/api/delete_sub_admin', {
          method: 'post',
           headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({'id':delid}),

        })

         
         Notification(
                  'success',
                  'success',
                  'Success'
                );

                setTimeout(function () { window.location.reload() }, 200);

       }
        if (handleOriginal) {
          handleOriginal()
        }
      }
    }
  }}
  />

        </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

