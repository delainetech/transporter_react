import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';
import fetch from 'cross-fetch';
import FormValidation from '../Forms/FormsWithValidation/AdminsEdit';
import Form from '../../components/uielements/form';
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 }
  }
};



export default class AntTable extends Component {
  constructor(props) {
   super(props);



   this.state = {
     data :  [
        {
            "time": "2018-01-30",
            "friend": {
                "HisarusersCount": 2,
                "NOIDAusersCount": 1
            }
        },
    ],

 columns : [
        {
            "id": 0,
            "registered_office_city": "",
            "Header": "Schedule date",
            "accessor": "time"
        },
        {
            "id": 1,
            "registered_office_city": "Hisar",
            "Header": "Hisar",
            "accessor": "d => d.friend.HisarusersCount"
        },
        {
            "id": 2,
            "registered_office_city": "NOIDA",
            "Header": "NOIDA",
            "accessor": "d => d.friend.NOIDAusersCount"
        }
    ],
   

   }


   
 
 }





  render() {

   const { rowStyle, colStyle, gutter } = basicStyle;
    var buttonAlign = {
      float:'right'
    }
    const headingStyle = {
      top:'2px'
    }
    return (

<LayoutWrapper>
        <PageHeader>Sub Admins</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={headingStyle}>
            <Box>



 <FormValidation />

<Helmet>
          <style>{`
  .dYawgl {
    margin-top: 0px !important;}
    .ReactTable {
      margin-top:20px;
    }
    .rt-tr {
      font-size:16px;
    }
 .rt-tbody{
  text-align:center;
 }
 .rt-tr{
  cursor:pointer;
 }
 .ghxaZn{
   margin-top: 0px !important;
 }
 .isoExampleWrapper{
   margin-top: 0px !important;
 }
 .ant-btn-primary{
 	width:20% ! important;
 	float:right;
 }
.ant-form-item-label { float: left;}
`}</style>
        </Helmet>

 </Box>
 </Col> 
        </Row>
      </LayoutWrapper>
    );
  }
}

