import React, { Component } from 'react';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import basicStyle from '../../config/basicStyle';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import Helmet from 'react-helmet';
import Select, { SelectOption } from '../../components/uielements/select';
import ContentHolder from '../../components/utility/contentHolder';
import Button from '../../components/uielements/button';
import Notification from '../../components/notification';
import fetch from 'cross-fetch';
import FormValidation from '../Forms/FormsWithValidation/Admins';
import Form from '../../components/uielements/form';
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 }
  }
};



export default class AntTable extends Component {


static defaultProps = {
    numberOfMonths: 2,
  };

 handleClick = () => {

  var thi = this;
   var filterCity = this.state.city;
   var filterEndDay = this.state.endDay;
   var filterStartDay = this.state.startDay;
    var pageName = this.props.match.params.name;
      var pageType = this.props.match.params.type;
var url ='http://104.131.28.228/transporter/public/api/CitiesFilter';
  if(pageName==='Deactivate' || pageName==='Approved')
  {
    var url = 'http://104.131.28.228/transporter/public/api/UserbyActionFilter'
  }
  fetch(url, {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({City:filterCity,endDay:filterEndDay,startDay:filterStartDay,type:pageType,page:pageName}) 


})
.then(function(data) {


  
  var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;



    thi.setState({
         data: coloum,
         columns : head,
         loading: false,
        
  });
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
   
    }







  constructor(props) {
   super(props);



   this.state = {
     data :  [
        {
            "time": "2018-01-30",
            "friend": {
                "HisarusersCount": 2,
                "NOIDAusersCount": 1
            }
        },
    ],

 columns : [
        {
            "id": 0,
            "registered_office_city": "",
            "Header": "Schedule date",
            "accessor": "time"
        },
        {
            "id": 1,
            "registered_office_city": "Hisar",
            "Header": "Hisar",
            "accessor": "d => d.friend.HisarusersCount"
        },
        {
            "id": 2,
            "registered_office_city": "NOIDA",
            "Header": "NOIDA",
            "accessor": "d => d.friend.NOIDAusersCount"
        }
    ],
   

   }


   
 
 }

 startDate = (day) => {
this.setState({startDay : day });

}

 endDate = (day) => {
this.setState({endDay : day });

}


logChange = (val) => {
  this.setState({city : val});
}

 
 componentWillMount() {
 var th = this;

      var pageName = this.props.match.params.name;
      var pageType = this.props.match.params.type;
  console.log(pageName);

  var url ='http://104.131.28.228/transporter/public/api/Cities';
  if(pageName==='Deactivate' || pageName==='Approved')
  {
    var url = 'http://104.131.28.228/transporter/public/api/UserbyAction'
  }
console.log(url);
      fetch(url, {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({type:pageType,page:pageName}) 


})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);

 var coloum = selectedFrequency.Coloumn;
 var head = selectedFrequency.header;
 var Select = selectedFrequency.Select;
   th.setState({
         data: coloum,
         columns : head,
  });

})
}


  render() {

   const { rowStyle, colStyle, gutter } = basicStyle;
    var buttonAlign = {
      float:'right'
    }
    const headingStyle = {
      top:'2px'
    }
    return (

<LayoutWrapper>
        <PageHeader>Sub Admins</PageHeader>
<Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={headingStyle}>
            <Box>



 <FormValidation />

<Helmet>
          <style>{`
  .dYawgl {
    margin-top: 0px !important;}
    .ReactTable {
      margin-top:20px;
    }
    .rt-tr {
      font-size:16px;
    }
 .rt-tbody{
  text-align:center;
 }
 .rt-tr{
  cursor:pointer;
 }
 .ghxaZn{
   margin-top: 0px !important;
 }
 .isoExampleWrapper{
   margin-top: 0px !important;
 }
 .ant-btn-primary{
 	width:20% ! important;
 	float:right;
 }
.ant-form-item-label { float: left;}
`}</style>
        </Helmet>

 </Box>
 </Col> 
        </Row>
      </LayoutWrapper>
    );
  }
}

