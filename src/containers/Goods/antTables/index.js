import React, { Component } from 'react';
import Tabs, { TabPane } from '../../../components/uielements/tabs';
import LayoutContentWrapper from '../../../components/utility/layoutWrapper';
import TableDemoStyle from './demo.style';
import fakeData from '../truck';
import { tableinfos } from './configs';
import * as TableViews from './tableViews/';
import Notification from '../../../components/notification';
import fetch from 'cross-fetch';

import FormValidation from '../../Forms/FormsWithValidation/Goods';


let dataList12 =null;

export default class AntTable extends Component {


  constructor() {
   super();
   this.state = {
      data:null
   }
 }


componentWillMount() {
 var th = this;

 



  fetch('http://104.131.28.228/transporter/public/api/goods_type')
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);

 var mainData = selectedFrequency.data;
var dataLength = mainData.length;
   dataList12 =new fakeData(dataLength,mainData);

 
  th.setState({
         data: selectedFrequency.data



     });







})
}


  renderTable(tableInfo) {
    let Component;
    switch (tableInfo.value) {
     
      case 'editView':
        Component = TableViews.EditView;
        break;
      
    }
    return <Component tableInfo={tableInfo} dataList12={dataList12} />;
  }
  render() {
 var align={
          textAlign:"center"
        }

    if (!this.state.data) {
            return <div />
        }


      

    return (
      <div>
      <FormValidation />

      <LayoutContentWrapper>
        <TableDemoStyle className="isoLayoutContent">
          <Tabs className="isoTableDisplayTab" style={align}>
            {tableinfos.map(tableInfo => (
              <TabPane tab={tableInfo.title} key={tableInfo.value}>
                {this.renderTable(tableInfo)}
              </TabPane>
            ))}
          </Tabs>
        </TableDemoStyle>
      </LayoutContentWrapper>
      </div>
    );
  }
}




export { TableViews, tableinfos, dataList12 };
