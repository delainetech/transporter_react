import React from 'react';
import clone from 'clone';
import IntlMessages from '../../../components/utility/intlMessages';
import { DateCell, ImageCell, LinkCell, TextCell } from './helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value);
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title: <IntlMessages id="antTable.title.image" />,
    key: 'avatar',
    width: '1%',
    className: 'isoImageCell',
    render: object => renderCell(object, 'ImageCell', 'avatar')
  },
  {
    title: <IntlMessages id="#" />,
    key: 'name',
    width: 100,
    render: object => renderCell(object, 'TextCell', '#')
  },
  {
    title: <IntlMessages id="Name" />,
    key: 'name',
    width: 100,
    render: object => renderCell(object, 'TextCell', 'Name')
  },
 
  {
    title: <IntlMessages id="antTable.title.street" />,
    key: 'street',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'street')
  },
  {
    title: <IntlMessages id="antTable.title.email" />,
    key: 'email',
    width: 200,
    render: object => renderCell(object, 'LinkCell', 'email')
  },
  {
    title: <IntlMessages id="antTable.title.dob" />,
    key: 'date',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'date')
  }
];



const truckcolumns = [
{
    title: <IntlMessages id="#" />,
    key: 'id',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'id')
  },
  
  {
    title: <IntlMessages id="Goods Type" />,
    key: 'name',
    width: 400,
    render: object => renderCell(object, 'TextCell', 'name')
  }
  

];



const smallColumns = [truckcolumns[0],truckcolumns[1]];
const sortColumns = [
  { ...columns[1], sorter: true },
  { ...columns[2], sorter: true },
  { ...columns[3], sorter: true },
  { ...columns[4], sorter: true }
];
const editColumns = [
  { ...columns[1], width: 300 },
  { ...columns[2], width: 300 },
  columns[3],
  columns[4]
];
const groupColumns = [
  columns[0],
  {
    title: 'Name',
    children: [columns[1], columns[2]]
  },
  {
    title: 'Address',
    children: [columns[3], columns[4]]
  }
];
const tableinfos = [
  {
    title: "All Good's Type",
    value: 'editView',
    columns: clone(smallColumns)
  }
];
export { columns, tableinfos,truckcolumns};
