import React, { Component } from 'react';
import clone from 'clone';
import TableWrapper from '../antTable.style';
import { EditableCell, DeleteCell } from '../helperCells';

import Notification from '../../../../components/notification';
import fetch from 'cross-fetch';


export default class CustomizedView extends Component {
  constructor(props) {
    super(props);
    this.onCellChange = this.onCellChange.bind(this);
    this.onDeleteCell = this.onDeleteCell.bind(this);
    this.state = {
      columns: this.createcolumns(clone(this.props.tableInfo.columns)),
      dataList12: this.props.dataList12.getAll(),
    };

  }
  createcolumns(columns) {

    const editColumnRender = (text, record, index) =>
      <EditableCell
        index={index}
        columnsKey={columns[2].key}
        value={text[columns[2].key]}
        onChange={this.onCellChange}
      />;
    columns[2].render = editColumnRender;
    const deleteColumn = {
      title: 'Actions',
      dataIndex: 'operation',
      render: (text, record, index) =>
        <DeleteCell index={index} onDeleteCell={this.onDeleteCell} />,
    };
    columns.push(deleteColumn);
    return columns;
  }
  onCellChange(value, columnsKey, index) {
    const { dataList12 } = this.state;
    dataList12[index][columnsKey] = value;
    this.setState({ dataList12 });
  }
  onDeleteCell = index => {


    var delid = this.state.dataList12[index]['id'];
  const { dataList12 } = this.state;
  console.log(dataList12);
    dataList12.splice(index, 1);
   
    this.setState({dataList12 });

     fetch('http://139.59.75.219/transport/public/api/delete_truck_type', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({'id':delid}),

})

 
 Notification(
          'success',
          'success',
          'Main Truck Type Deleted'
        );
 
}
  render() {
    const { columns, dataList12 } = this.state;

    return (
      <TableWrapper
        columns={columns}
        dataSource={dataList12}
        className="isoEditableTable"
      />
    );
  }
}
