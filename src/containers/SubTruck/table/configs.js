import React from 'react';
import clone from 'clone';
import IntlMessages from '../../../components/utility/intlMessages';
import { DateCell, ImageCell, LinkCell, TextCell } from './helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value);
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title:"antTable.title.image",
    key: 'avatar',
    width: '1%',
    className: 'isoImageCell',
    render: object => renderCell(object, 'ImageCell', 'avatar')
  },
  {
    title: "#",
    key: 'name',
    width: 100,
    render: object => renderCell(object, 'TextCell', '#')
  },
  {
    title:"Name",
    key: 'name',
    width: 100,
    render: object => renderCell(object, 'TextCell', 'Name')
  },
 
  {
    title:"antTable.title.street",
    key: 'street',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'street')
  },
  {
    title: "antTable.title.email",
    key: 'email',
    width: 200,
    render: object => renderCell(object, 'LinkCell', 'email')
  },
  {
    title:"antTable.title.dob",
    key: 'date',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'date')
  }
];



const truckcolumns = [
{
    title:"Id",
    key: 'id',
    width: 100,
    render: object => renderCell(object, 'TextCell', '#')
  },
  {
    title:"Truck Category",
    key: 'truckName',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'truckName')
  },
  {
    title: "Truck Type",
    key: 'name',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'name')
  }
  

];



const smallColumns = [truckcolumns[0],truckcolumns[1],truckcolumns[2]];
const sortColumns = [
  { ...columns[1], sorter: true },
  { ...columns[2], sorter: true },
  { ...columns[3], sorter: true },
  { ...columns[4], sorter: true }
];
const editColumns = [
  { ...columns[1], width: 300 },
  { ...columns[2], width: 300 },
  columns[3],
  columns[4]
];
const groupColumns = [
  columns[0],
  {
    title: 'Name',
    children: [columns[1], columns[2]]
  },
  {
    title: 'Address',
    children: [columns[3], columns[4]]
  }
];
const tableinfos = [
  {
    title: 'All Truck Types',
    value: 'editView',
    columns: clone(smallColumns)
  }
];
export { columns, tableinfos,truckcolumns};
