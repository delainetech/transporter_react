import React, { Component } from 'react';
import { connect } from 'react-redux';
import contactAction from '../../redux/contacts/actions';
import { Col,Layout } from 'antd';
import Button from '../../components/uielements/button';
import ContentHolder from '../../components/utility/contentHolder';
import { ContactsWrapper } from './contacts.style';
import fetch from 'cross-fetch';
import ModalStyle from '../Feedback/Modal/modal.style';
import Notification from '../../components/notification';
import Modals from '../../components/feedback/modal';
import WithDirection from '../../config/withDirection';
import Helmet from 'react-helmet';

const isoModal = ModalStyle(Modals);

const confirm = Modals.confirm;


const baseUrl = 'http://104.131.28.228/transporter/public/userdocuments/';
const {
  changeContact,
  addContact,
  editContact,
  deleteContact,
  viewChange
} = contactAction;

const { Content } = Layout;
let userData = null;
let idImages='';
let vehicleImages='';
let idbackimage='';
let idfrontimage='';
let vehiclefrontimage='';
let vehiclebackimage='';
 let th = '';

class Contacts extends Component {
 


 
  constructor(props)
  {
     super(props);
      th = this;
     this.state = {
      data:null,
      userId:  this.props.match.params.id,
      message : '',
   }
   

    var userId = this.props.match.params.id;

   
     fetch('http://104.131.28.228/transporter/public/api/usersDetail', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({id:userId}),

})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);
 if(selectedFrequency.message=='error')
 {
  Notification(
          'error',
          'error',
          'No Data Found'
        );

 }
 else{
  var mainData = selectedFrequency.data[0];
   userData = mainData;
console.log(userData);
  th.setState({
         data: mainData
     });


}


})

 
  }


 uploadDocument = () => {

   var userId = this.state.userId;

  window.location.href = "http://localhost:3000/dashboard/User-Edit/"+userId;
}

  render() {


function handleTitleChange(e){
  
  var message  = e.target.value;

  this.setState({message:message});
}

function showConfirm() {
  confirm({
    title: 'Send Message To This User',
    content:
 
      <input type="text" className="form-control" placeholder="enter message here" name="message"    onChange={handleTitleChange.bind(this)}/> 
  ,
    onOk() {
        var message = th.state.message;
        var userId = th.state.userId;
  fetch('http://104.131.28.228/transporter/public/api/edit_admin_message', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({id:userId,message:message}),

})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);
 if(selectedFrequency.message=='error')
 {
  Notification(
          'error',
          'error',
          'No Data Found'
        );

 }
 else{
  
  Notification(
          'success',
          'success',
          'Message Sent Successfully'
        );

}


})
 return new Promise((resolve, reject) => {
        setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
      }).catch(() => {});
    
    },
    onCancel() {},
    okText: 'Submit',
    cancelText: 'Cancel',
  });
}




function handleClick(e) {

  var editid = userData.id;
      fetch('http://104.131.28.228/transporter/public/api/userApprove', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({'id':editid}),

}).then(function(data) {
  
const selectedFrequency = JSON.parse(data._bodyText);
console.log(selectedFrequency);
if(selectedFrequency.message=='success' )
{
 var message = selectedFrequency.responce;
 if(message==0)
 {
  Notification(
          'success',
          'success',
          "User Disapprove Successfully"
        );
 }
 else
 {
  Notification(
          'success',
          'success',
          "user Successfully Approved"
        );
 }
 

 setTimeout(function () { window.location.reload() }, 1000);
} 
else
{
  Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
} 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
  }
  const img={
    height:'150px',
    width : 'auto',
    paddingLeft:'10px'
  }
const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
    };
    
    const colStyle = {
      fontSize: '14px',
      padding:'10px 0px 0px',
      borderBottom : '1px dashed #a6a6a6',
      marginBottom: '20px',
    };

    const button = {
      float:'right'
    }
    const bodyStyle = {
      padding:'20px',
      fontWeight:'500'
    }

    const headStyle = {
      fontWeight:'900',
      fontSize:'15px'
    }
    const card = {

      margin: '20px 0px 0px 0px',
      color : '#4482FF',
    }
    const gutter = 16;
    
    if (!this.state.data) {
            return <div />
        }
  
   idImages = JSON.parse(userData.id_images);
   vehicleImages = JSON.parse(userData.owned_vehicle_images);
 
    if(idImages!=null)
    {
       idfrontimage = baseUrl+idImages[0].image;
       idbackimage = baseUrl+idImages[1].image;
    }


    if(vehicleImages!=null)
    {
       vehiclefrontimage = baseUrl+vehicleImages[0].image;
       vehiclebackimage = baseUrl+vehicleImages[1].image;
    }
    if(userData.userStatus=='No')
    {
      var btnclass = 'btn-danger';
      var btnvalue =  'User Approvel Pending'
    }
    else
    {
      var btnclass = 'btn-success';
      var btnvalue =  'User Approved'
    }

    return (
       
      <div>
      <ContactsWrapper
        className="isomorphicContacts"
        style={{ background: 'none' }}
      >
        
        <Layout className="isoContactBoxWrapper" style={bodyStyle}>
         
         {/* <h1>{userData.email}</h1> */}


         <div className="container">
<Helmet>
          <style>{`
            .koDQW{
              margin-top: 0px !important;
               margin-bottom: 50px !important;
            }
.ant-modal{
  width:550px !important;
}
 .gmoydk{
   margin-top: 0px !important;
   margin-bottom: 30px !important;
 }
 h4 {
  padding:10px 0px !important;
 }
 img {
    max-width: 300px  !important;
    max-height : 200px  !important;
 }
`}</style>
        </Helmet>

       
       <Col md={24} sm={24} xs={24} >
       <Col md={8} sm={8} xs={8}>
         <ContentHolder>
                <Button onClick={showConfirm.bind(this)}>
                 
                    Send Message To Transporter
                 
                </Button>
              </ContentHolder>
               </Col>
               <Col md={8} sm={8} xs={8}>
               <Button onClick={this.uploadDocument}>
                 Upload User's Document
                </Button>
               </Col>
              <Col md={8} sm={8} xs={8}>
         <button style={button} onClick={handleClick} className={"btn "+btnclass}> {btnvalue}</button>
       </Col>
       </Col>

        <h4 style={card}>Personal Information</h4>
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Name :
            </Col>
            <Col md={8} sm={8} xs={8} >
              {userData.name}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle} >
              Unique Id:
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.uuid}
            </Col>
        </Col>


        <Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Email :
            </Col>
            <Col md={8} sm={8} xs={8} >
              {userData.email}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              Phone Number
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.phone_no}
            </Col>
        </Col>




        <Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Language :
            </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.Language}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              Refferel Code:
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.refferel_code}
            </Col>
        </Col>


<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              User Type :
            </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.User_type}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              User since:
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.user_since} Days
            </Col>
        </Col>

<h4 style={card}>Company Information</h4>
<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Company Name :
            </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.company_name}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              Registered Office City:
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.registered_office_city} 
            </Col>
        </Col>


<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Branch : 
            </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.branches}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              I Am A :
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.i_am_a_full} 
            </Col>
        </Col>

<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={6} sm={4} xs={4} style={headStyle}>
              Company Address Proof Type :
            </Col>
            <Col md={6} sm={4} xs={4}>
              {userData.company_address_proof}
            </Col>
   

            <Col md={6} sm={4} xs={4} style={headStyle}>
              Company Address proof Value : 
           </Col>
            <Col md={6} sm={4} xs={4}>
              {userData.company_address_proof_value} 
            </Col>
        </Col>


<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={2} xs={2} style={headStyle}>
              Personal Id Type :
            </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.type_of_id}
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              Any Owned Vehicle: 
           </Col>
            <Col md={8} sm={8} xs={8}>
              {userData.owned_vehicle} 
            </Col>
        </Col>


<h4 style={card}>Uploaded Document Images</h4>
<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={4} xs={4} style={headStyle} >
              Personal Id Images Front 
            </Col>
            <Col md={8} sm={8} xs={8}>

             <img src={idfrontimage} className="img-circle img-responsive" style={img} />
             
            </Col>
   

            <Col md={4} sm={2} xs={2} style={headStyle}>
              Personal Id Images Back : 
           </Col>
            <Col md={8} sm={8} xs={8}>
             
                <img src={idbackimage} className="img-circle img-responsive" style={img} />
            </Col>
        </Col>


<Col md={24} sm={24} xs={24} style={colStyle}>
            <Col md={4} sm={4} xs={4} style={headStyle}>
              Vehicle Image Front
            </Col>
            <Col md={8} sm={8} xs={8}>
              <img src={vehiclefrontimage} className="img-circle img-responsive" style={img} />
            </Col>
   

            <Col md={4} sm={4} xs={4} style={headStyle}>
               Vehicle Image Back 
           </Col>
            <Col md={8} sm={8} xs={8}>
              <img src={vehiclebackimage} className="img-circle img-responsive" style={img} />
            </Col>
        </Col>

           </div>
        </Layout>

      </ContactsWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { contacts, seectedId, editView } = state.Contacts.toJS();
  return {
    contacts,
    seectedId,
    editView
  };
}
export default connect(mapStateToProps, {
  changeContact,
  addContact,
  editContact,
  deleteContact,
  viewChange
})(Contacts);
