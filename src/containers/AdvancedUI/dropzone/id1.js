import React , { Component } from 'react';
import Dropzone from '../../../components/uielements/dropzone.js';
import { notification } from '../../../components';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import ContentHolder from '../../../components/utility/contentHolder';
import DropzoneWrapper from './dropzone.style';
import Helmet from 'react-helmet';
export default class extends Component {

  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: ''
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  _handleSubmit(e) {
    e.preventDefault();
    var img = this.state.file;
 
 console.log(img);
    var formData = new FormData();

    formData.append("id", "10");


    formData.append('img',img);

    

console.log(formData);
     fetch('http://104.131.28.228/transporter/public/api/file', {
  method: 'post',
   headers: {   'Accept': 'application/json',
      'Content-Type': 'multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d' }
    ,
  body: formData 

})
.then(function(data) {
  
  console.log(data);
 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    console.log(file);
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }

   render() {
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} />);
    }
 
  return (
  <div>
  <Helmet>
          <style>{`
    img{
      max-height:240px !important;
      max-width:300px ! important;
    }
    .form-control1{
      padding:2px 10px !important;
      font-size: 13px !important ;
      display: inline-block;
      width: 80%;
    }
    .btn {
      font-size:13px !important;
    }
`}</style>
        </Helmet>

<form onSubmit={this._handleSubmit}>
          <input type="file" className="form-control form-control1" onChange={this._handleImageChange} />
          <button type="submit" class="btn btn-info" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        <div className="img-responsive styl" >{$imagePreview}</div>
        </div>
  )
}
}
