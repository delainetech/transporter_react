import React from 'react';
import clone from 'clone';
import IntlMessages from '../../../components/utility/intlMessages';
import { DateCell, ImageCell, LinkCell, TextCell } from './helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value);
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title: <IntlMessages id="Id" />,
    key: 'id',
    width: 100,
    render: object => renderCell(object, 'TextCell', 'id')
  },
  {
    title: <IntlMessages id="Unique Id" />,
    key: 'uuid',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'uuid')
  },
  {
    title: <IntlMessages id="Name " />,
    key: 'name',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'name')
  },
  {
    title: <IntlMessages id="Email Id" />,
    key: 'email',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'email')
  },
  {
    title: <IntlMessages id="Phone No" />,
    key: 'phone_no',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'phone_no')
  },
  {
    title: <IntlMessages id="Last Login" />,
    key: 'last_login',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'last_login')
  },

  {
    title: <IntlMessages id="User Active" />,
    key: 'userStatus',
    width: 100,
    render: object => renderCell(object, 'TestCell', 'userStatus')
  }
];
const smallColumns = [columns[1], columns[2], columns[3], columns[4], columns[5], columns[6]];
const sortColumns = [
  { ...columns[1], sorter: true },
  { ...columns[2], sorter: true },
  { ...columns[3], sorter: true },
  { ...columns[4], sorter: true }
];
const editColumns = [
  { ...columns[1], width: 300 },
  { ...columns[2], width: 300 },
  columns[3],
  columns[4]
];
const groupColumns = [
  columns[0],
  {
    title: 'Name',
    children: [columns[1], columns[2]]
  },
  {
    title: 'Address',
    children: [columns[3], columns[4]]
  }
];
const tableinfos = [
  
  {
    title: 'All Users',
    value: 'filterView',
    columns: clone(smallColumns)
  }
];
export { columns, tableinfos };
