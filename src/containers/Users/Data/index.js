import React, { Component } from 'react';
import Tabs, { TabPane } from '../../../components/uielements/tabs';
import LayoutContentWrapper from '../../../components/utility/layoutWrapper';
import TableDemoStyle from './demo.style';
import fakeData from '../truck';
import { tableinfos } from './configs';
import * as TableViews from './tableViews/';
import Notification from '../../../components/notification';
import fetch from 'cross-fetch';
import Helmet from 'react-helmet';
import FormValidation from '../../Forms/FormsWithValidation/Addtruck';
import datePicker from '../../AdvancedUI/ReactDates/reactDates.js'
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import Switch from '../../../components/uielements/switch';
import Form from '../../../components/uielements/form';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import ContentHolder from '../../../components/utility/contentHolder';

import basicStyle from '../../../config/basicStyle';


let dataList =null;
const FormItem = Form.Item;
export default class AntTable extends Component {


  constructor(props) {
     super(props);
   this.state = {
      data:null
   }
 }



componentWillMount() {
 var th = this;
     var pageName = this.props.match.params.name;
      
fetch('http://104.131.28.228/transporter/public/api/usersData', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({page:pageName,filter:0}),

})
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);
 if(selectedFrequency.message=='error')
 {
  Notification(
          'error',
          'error',
          'No Data Found'
        );

 }
 else{
 var mainData = selectedFrequency.data;
var dataLength = mainData.length;
   dataList =new fakeData(dataLength,mainData);
  th.setState({
         data: selectedFrequency.data
     });


}
})
}



  
  renderTable(tableInfo) {
    let Component;
    switch (tableInfo.value) {
    
      case 'filterView':
        Component = TableViews.FilterView;
        break;
      
      default:
        Component = TableViews.FilterView;
    }
    return <Component tableInfo={tableInfo} dataList={dataList} />;
  }

   
  render() {
const { rowStyle, colStyle, gutter } = basicStyle;
     if (!this.state.data) {
            return <div />
        }

const rowHeight = {
  

}
    return (

 
        <div>
      <LayoutContentWrapper>

<Helmet>
          <style>{`
 .ant-tabs-bar{
  display:none
 }
 .rNXmZ{
  margin:0px !important;
  width : 90% ! important;
 }
`}</style>
        </Helmet>
        <TableDemoStyle className="isoLayoutContent">
       <PageHeader>Users Data</PageHeader>

          <Tabs className="isoTableDisplayTab">
            {tableinfos.map(tableInfo => (
              <TabPane tab={tableInfo.title} key={tableInfo.value}>
                {this.renderTable(tableInfo)}
              </TabPane>
            ))}
          </Tabs>
        </TableDemoStyle>
      </LayoutContentWrapper>
      </div>
    );
  }
}
export { TableViews, tableinfos, dataList };
