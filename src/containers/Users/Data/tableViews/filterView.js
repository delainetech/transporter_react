import React, { Component } from 'react';
import { Icon } from 'antd';
import Input from '../../../../components/uielements/input';
import Button from '../../../../components/uielements/button';
import TableWrapper from '../antTable.style';

import Notification from '../../../../components/notification';
import fetch from 'cross-fetch';

import clone from 'clone';
import { EditableCell, DeleteCell, EditCell } from '../helperCells';


export default class FilterView extends Component {
  constructor(props) {
    super(props);

    this.onSearch = this.onSearch.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onCellChange = this.onCellChange.bind(this);
    this.onDeleteCell = this.onDeleteCell.bind(this);
    this.state = {
      columns: this.createcolumns(clone(this.props.tableInfo.columns)),
      dataList: this.props.dataList.getAll(),
      filterDropdownVisible: false,
      searchText: '',
      filtered: false
    };


  }

  createcolumns(columns) {
   
    const deleteColumn = {
      title: 'Action',
      dataIndex: 'operation',
      render: (text, record, index) =>
        <DeleteCell index={index} onDeleteCell={this.onDeleteCell} />,
    };

    const viewDetail = {
      title: 'View Detail',
      render: (text, record, index) =>
        <EditCell index={index}  onDeleteCell={this.onCellChange} />,
    };

    columns.push(deleteColumn);
    columns.push(viewDetail);
    return columns;
}
  onSearch() {
    let { searchText } = this.state;
    searchText = searchText.toUpperCase();
    const dataList = this.props.dataList.getAll()
      .filter(data => data['phone_no'].toUpperCase().includes(searchText));
    this.setState({
      dataList,
      filterDropdownVisible: false,
      searchText: '',
      filtered: false
    });
  }
  onInputChange(event) {
    this.setState({ searchText: event.target.value });
  }
  onCellChange(index) {
  var preview_id = this.state.dataList[index]['id'];

  window.location.href = "http://localhost:3000/dashboard/User-profile/"+preview_id;
  }
  onDeleteCell = index => {

    var delid = this.state.dataList[index]['id'];

   const { dataList } = this.state;

    dataList.splice(index, 1);
   
    this.setState({dataList });
 fetch('http://104.131.28.228/transporter/public/api/usersDelete', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify({'id':delid}),

})

 
 Notification(
          'success',
          'success',
          'User Deleted Successfully'
        );


    // const { dataList } = this.state;
    // dataList.splice(index, 1);
    // this.setState({ dataList });
  };

  render() {
    const filterDropdown = (
      <div className="isoTableSearchBox">
        <Input
          id="tableFilterInput"
          ref={ele => (this.searchInput = ele)}
          placeholder="Search name"
          value={this.state.searchText}
          onChange={this.onInputChange}
          onPressEnter={this.onSearch}
        />
        <Button type="primary" onClick={this.onSearch}>
          Search
        </Button>
      </div>
    );
    const columns = this.state.columns;
    columns[3] = {
      ...columns[3],
      filterDropdown,
      filterIcon: (
        <Icon
          type="search"
          style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }}
        />
      ),
      filterDropdownVisible: this.state.filterDropdownVisible,
      onFilterDropdownVisibleChange: visible =>
        this.setState({ filterDropdownVisible: visible }, () =>
          document.getElementById('tableFilterInput').focus()
        )
    };

    
    return (
      <TableWrapper
        columns={columns}
        onChange={this.onChange}
        dataSource={this.state.dataList}
        className="isoSearchableTable"
      />
    );
  }
}
