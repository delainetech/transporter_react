import React from 'react';
import { Input } from 'antd';
import Form from '../../../components/uielements/form';


import Button from '../../../components/uielements/button';

import Notification from '../../../components/notification';
import fetch from 'cross-fetch';

const FormItem = Form.Item;





class FormWIthSubmissionButton extends React.Component {

  state = {
    confirmDirty: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // Notification(
        //   'success',
        //   'Success',
      
        // );


         fetch('http://104.131.28.228/transporter/public/api/login', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify(values),

})
.then(function(data) {
  
  const selectedFrequency = JSON.parse(data._bodyText);

if(selectedFrequency.message=='success')
{
 
// window.sessionStorage.setItem("isLoggedIn", "true");
 sessionStorage.setItem('isLoggedIn', 'true');
 Notification(
          'success',
          'success',
          'Sucessfully Logged In'
        );


 setTimeout(function () {window.location = "/dashboard" }, 500);



} 
else
{
  Notification(
          'error',
          'error',
          'Please Fill Correct Info'
        );
} 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})

    
      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    var style={
      width:'100%'
    }
    return (
      <Form onSubmit={this.handleSubmit}>
        
        <FormItem {...formItemLayout} label="USERNAME" className="isoInputWrapper" hasFeedback>
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
          })(<Input name="email" id="email" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="PASSWORD" hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                validator: this.checkConfirm,
              },
            ],
          })(<Input type="password" />)}
        </FormItem>
      
        
        <FormItem {...tailFormItemLayout}>
          <Button style={style} type="primary" htmlType="submit">
            LOG IN 
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
