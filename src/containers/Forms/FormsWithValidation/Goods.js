import React from 'react';
import { Input } from 'antd';
import Form from '../../../components/uielements/form';
import Button from '../../../components/uielements/button';

import Notification from '../../../components/notification';
import fetch from 'cross-fetch';

const FormItem = Form.Item;
 



class FormWIthSubmissionButton extends React.Component {


  state = {
    confirmDirty: false,
  };







  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
       

         fetch('http://104.131.28.228/transporter/public/api/add_goods_type', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify(values),

})
.then(function(data) {
  
  const selectedFrequency = JSON.parse(data._bodyText);
if(selectedFrequency.message=='success')
{
 

 Notification(
          'success',
          'success',
          "Sucessfully Good's Type  Added"
        );


 //window.location.reload()
 setTimeout(function () { window.location.reload() }, 1000);


} 
else
{
  Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
} 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})

    
      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  
  

  render() {

     


    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    var style={
      width:'100',
      float: "right"
    }
    var back = {
      backgroundColor:"#FFF",
      padding:"50px 0px 10px",
      margin:"50px 42px 0px"
    }

    var Head = {
      paddingBottom:"15px",
      color:"#4482ff",
      textAlign : "center"
    }
    var selectValue = {
       width: '100%',
     
    }
    var contenthold = {
      marginTop:'1px'
    }
    return (


      <div style={back}>
      <h1 style={Head}> Good's Data</h1>
      <Form onSubmit={this.handleSubmit}  >
        


              
        <FormItem {...formItemLayout} label="Enter Good Type" className="isoInputWrapper" hasFeedback>
          {getFieldDecorator('name', {
            
          })(<Input name="name" id="email" />)}
        </FormItem>

       
        
      
        
        <FormItem {...tailFormItemLayout}>
          <Button style={style} type="primary" htmlType="submit">
            Add Goods Type
          </Button>
        </FormItem>
      </Form>
      </div>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
