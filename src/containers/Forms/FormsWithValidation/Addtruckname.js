import React from 'react';
import { Input } from 'antd';
import Form from '../../../components/uielements/form';
import Select, { SelectOption } from '../../../components/uielements/select';
import Button from '../../../components/uielements/button';

import Notification from '../../../components/notification';
import fetch from 'cross-fetch';

const FormItem = Form.Item;


class FormWIthSubmissionButton extends React.Component {

 constructor() {
   super();
   

   this.state = {
      values: [
        { name: 'One', id: 1 },
        { name: 'Two', id: 2 },
        { name: 'Three', id: 3 },
        { name: 'four', id: 4 }
      ]
    };

  
 }
 

componentWillMount() {
 var th = this;
fetch('http://104.131.28.228/transporter/public/api/truck_type')
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);

 var mainData = selectedFrequency.data;



 
  th.setState({
         values: mainData



     });

})
}





  handleSubmit = e => {
    e.preventDefault();

   
    this.props.form.validateFieldsAndScroll((err, values) => {

      if (!err) {


         fetch('http://104.131.28.228/transporter/public/api/add_sub_truck_type', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify(values),

})
.then(function(data) {
  
  const selectedFrequency = JSON.parse(data._bodyText);
if(selectedFrequency.message=='success')
{
 

 Notification(
          'success',
          'success',
          'Sucessfully Truck Type Added'
        );


 //window.location.reload()
 setTimeout(function () { window.location.reload() }, 2000);


} 
else
{
  Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
} 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})

    
      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  
  

  render() {

 let optionTemplate = this.state.values.map(v => (
      <option value={v.id}>{v.name}</option>
    ));

    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    var style={
      width:'100',
      float: "right"
    }
    var back = {
      backgroundColor:"#FFF",
      padding:"50px 0px 10px",
      margin:"50px 42px 0px"
    }

    var Head = {
      paddingBottom:"15px",
      color:"#4482ff",
      textAlign : "center"
    }
    var selectValue = {
       width: '100%',
     
    }
    var contenthold = {
      marginTop:'1px'
    }
    return (


      <div style={back}>
      <h1 style={Head}> Truck's Name</h1>
      <Form onSubmit={this.handleSubmit}  >
        
         <FormItem {...formItemLayout} label=" Select Truck Category" className="isoInputWrapper" hasFeedback>
          {getFieldDecorator('truck_id', {
            
          })(
                <Select
                  defaultValue="Select Truck Category First"
                  onChange={this.handleChange}
                  style={selectValue}
                >
                    {optionTemplate}
                
                </Select>
             )}
        </FormItem>


              
        <FormItem {...formItemLayout} label="Truck Name" className="isoInputWrapper" hasFeedback>
          {getFieldDecorator('name', {
            
          })(<Input name="name" id="email" />)}
        </FormItem>

       
        
      
        
        <FormItem {...tailFormItemLayout}>
          <Button style={style} type="primary" htmlType="submit">
            Add New truck 
          </Button>
        </FormItem>
      </Form>
      </div>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
