import React from 'react';
import { Input } from 'antd';
import Form from '../../../components/uielements/form';
import { Row, Col } from 'antd';
import Checkbox from '../../../components/uielements/checkbox';
import Button from '../../../components/uielements/button';

import Notification from '../../../components/notification';
import fetch from 'cross-fetch';

const FormItem = Form.Item;





class FormWIthSubmissionButton extends React.Component {

  state = {
    confirmDirty: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values);


         fetch('http://104.131.28.228/transporter/public/api/add_sub_admin', {
  method: 'post',
   headers: {'Content-Type': 'application/json'},
  body: JSON.stringify(values),

})
.then(function(data) {
  
  console.log(data);
  const selectedFrequency = JSON.parse(data._bodyText);

if(selectedFrequency.message=='success')
{
  Notification(
          'success',
          'success',
          'Sucessfully Admin Created'
        );


 setTimeout(function () {window.location = "dashboard/SubAdmins" }, 500);



} 
else
{
  Notification(
          'error',
          'error',
          selectedFrequency.message
        );
} 
}, function(error) {
Notification(
          'error',
          'error',
          'Something Went Wrong Please try again'
        );
})

    
      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {

     const headingStyle = {
      top:'2px'
    }
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
   
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem {...formItemLayout} label="Name" hasFeedback>
          {getFieldDecorator('name', {
            rules: [
              
              {
                required: true,
                message: 'Please Enter Sub Admin Name',
              }
            ],
          })(<Input name="name" id="name" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Phone No." hasFeedback>
          {getFieldDecorator('phone_no', {
            rules: [
             
              {
                required: true,
                message: 'Please Enter Admin Phone No!',
              },
            ],
          })(<Input name="phone" id="phone" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Email" className="isoInputWrapper" hasFeedback>
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input Admin  E-mail!',
              },
            ],
          })(<Input name="email" id="email" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="PASSWORD" hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Please input your password!',
              }
            ],
          })(<Input type="password" />)}
        </FormItem>
      <FormItem {...formItemLayout} label="Confirm Password" hasFeedback>
          {getFieldDecorator('confirm', {
            rules: [
              {
                required: true,
                message: 'Please confirm your password!',
              },
            ],
          })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
        </FormItem>


        <Col md={24} sm={24} xs={24} style={headingStyle}>

          <Col md={12} sm={12} xs={12} style={headingStyle}>
              <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Account', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Account</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Sub-Admins', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Sub-Admins</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Operating location', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Operating location</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Loads', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Loads</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Truck', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Truck</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Fare management', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Fare management</Checkbox>
          )}
        </FormItem>
          </Col>

          <Col md={12} sm={12} xs={12} style={headingStyle}>
            <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Complaints', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Complaints</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Notification', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Notification</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Rating', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Rating</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Referrals', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Referrals</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Reports', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Reports</Checkbox>
          )}
        </FormItem>
         <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('Transaction details', {
            valuePropName: 'checked',
            
          })(
            <Checkbox>Transaction details</Checkbox>
          )}
        </FormItem>
          </Col>

        </Col>
         
         
        <FormItem {...tailFormItemLayout}>
          <Button  type="primary" htmlType="submit">
            Create Admin
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
