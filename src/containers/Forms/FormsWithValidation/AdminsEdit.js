import React from 'react';
import {Input} from 'antd';
import Form from '../../../components/uielements/form';
import {Row, Col} from 'antd';
import Checkbox from '../../../components/uielements/checkbox';
import Button from '../../../components/uielements/button';
import Notification from '../../../components/notification';
import fetch from 'cross-fetch';
import Helmet from 'react-helmet';
import ContentHolder from '../../../components/utility/contentHolder';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import basicStyle from '../../../config/basicStyle';
const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 5
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 12
    }
  }
};

class FormWIthSubmissionButton extends React.Component {

  constructor(props) {
    super(props);
    let id = this.props.match.params.id;

    this.state = {
      data: null,
      id: id
    }
  }

  componentWillMount() {
    var th = this;

    var eid = th.state.id;

    var url = "http://104.131.28.228/transporter/public/api/edit_admin/" + eid;

    fetch(url).then(function(data) {

      var selectedFrequency = JSON.parse(data._bodyText);

      th.setState({data: selectedFrequency.data});
    
    })

  }

  state = {
    confirmDirty: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values);

        fetch('http://104.131.28.228/transporter/public/api/edit_sub_admin', {
          method: 'post',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(values)
        }).then(function(data) {

          const selectedFrequency = JSON.parse(data._bodyText);

          if (selectedFrequency.message == 'success') {
            Notification('success', 'success', 'Sucessfully Admin Edited');

            if(process.env.NODE_ENV !== "production")
            {
                setTimeout(function() {
                window.location.href = "http://localhost:3000/dashboard/SubAdmins"
            },  500);
            }
            else
            {
              setTimeout(function() {
               window.location = "http://104.131.28.228:9000/dashboard/SubAdmins"
            }, 500);
            }
            

          } else {
            Notification('error', 'error', selectedFrequency.message);
          }
        }, function(error) {
          Notification('error', 'error', 'Something Went Wrong Please try again');
        })

      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({
      confirmDirty: this.state.confirmDirty || !!value
    });
  };
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], {force: true});
    }
    callback();
  };

  render() {

    if (!this.state.data) {
      return <div/>
    }


    var userdata = this.state.data;

    var email = String(userdata[0].email);
    var pages = JSON.parse(userdata[0].pages);
    
    var Account ='';
    var Fare_management ='';
    var Loads ='';
    var Operating_location ='';
    var Referrals ='';
    var Reports ='';
    var Sub_Admins ='';
    var Transaction_details ='';
    var Truck ='';
    var Complaints ='';
    var Notification ='';
    var Rating ='';

    for (var i = 0; i < pages.length; i++) {
 


if(pages[i]=='Account'){
  Account = 'True';
}
else if(pages[i]=='Fare management'){
  Fare_management = 'True';
}
else if(pages[i]=='Loads'){
  Loads = 'True';
}
else if(pages[i]=='Operating location'){
  Operating_location = 'True';
}
else if(pages[i]=='Referrals'){
  Referrals = 'True';
}
else if(pages[i]=='Reports'){
  Reports = 'True';
}
else if(pages[i]=='Sub-Admins'){
  Sub_Admins = 'True';
}
else if(pages[i]=='Transaction details'){
  Transaction_details = 'True';
}
else if(pages[i]=='Truck'){
  Truck = 'True';
}
else if(pages[i]=='Complaints'){
  Complaints = 'True';
}
else if(pages[i]=='Notification'){
  Notification = 'True';
}
else if(pages[i]=='Rating'){
  Rating = 'True';
}

  

}
    var name = String(userdata[0].name);

    var phone = String(userdata[0].phone);

    const headingStyle = {
      top: '2px'
    }
    const {getFieldDecorator} = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 6
        }
      },
      wrapperCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 14
        }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 14,
          offset: 6
        }
      }
    };
    const {rowStyle, colStyle, gutter} = basicStyle;
    var buttonAlign = {
      float: 'right'
    }

    return (<LayoutWrapper>
      <PageHeader>Sub Admins</PageHeader>
      <Row style={rowStyle} gutter={gutter} justify="start">
        <Col md={24} sm={24} xs={24} style={headingStyle}>
          <Box>

            <Form onSubmit={this.handleSubmit}>
              <FormItem {...formItemLayout} label="Name" hasFeedback="hasFeedback">
                {
                  getFieldDecorator('name', {
                    initialValue: name,
                    rules: [

                      {
                        required: true,
                        message: 'Please Enter Sub Admin Name'
                      }
                    ]
                  })(<Input name="name" id="name"/>)
                }
              </FormItem>

              <FormItem {...formItemLayout} label="Phone No." hasFeedback="hasFeedback">
                {
                  getFieldDecorator('phone_no', {
                    initialValue: phone,
                    rules: [

                      {
                        required: true,
                        message: 'Please Enter Admin Phone No!'
                      }
                    ]
                  })(<Input name="phone" id="phone"/>)
                }
              </FormItem>

              <FormItem {...formItemLayout} label="Email" className="isoInputWrapper" hasFeedback="hasFeedback">
                {
                  getFieldDecorator('email', {
                    initialValue: email,
                    rules: [
                      {
                        type: 'email',
                        message: 'The input is not valid E-mail!'
                      }, {
                        required: true,
                        message: 'Please input Admin  E-mail!'
                      }
                    ]
                  })(<Input readonly='readonly' name="email" id="email"/>)
                }
              </FormItem>
              <FormItem {...formItemLayout} label="Password" hasFeedback="hasFeedback">
                {
                  getFieldDecorator('password', {

                    rules: [
                      {
                      }
                    ]
                  })(<Input type="password" placeholder="Please Enter if you want to change"/>)
                }
              </FormItem>
              <FormItem {...formItemLayout} label="Confirm Password" hasFeedback="hasFeedback">
                {
                  getFieldDecorator('confirm', {
                    rules: [
                      {
                      
                      }
                    ]
                  })(<Input type="password" onBlur={this.handleConfirmBlur} placeholder="Please Enter if you want to change"/>)
                }
              </FormItem>

              <Col md={24} sm={24} xs={24} style={headingStyle}>

                <Col md={12} sm={12} xs={12} style={headingStyle}>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Account', 
                      {
                        initialValue: Account,
                        valuePropName: 'checked'
                       
                      })
                      (<Checkbox>Account</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Sub-Admins',
                     {
                        initialValue: Sub_Admins,
                        valuePropName: 'checked'
                       
                      }
                     )
                    (<Checkbox >Sub-Admins</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Operating location', {
                        initialValue: Operating_location,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Operating location</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Loads',{
                        initialValue: Loads,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Loads</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Truck', {
                        initialValue: Truck,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Truck</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Fare management', {
                        initialValue: Fare_management,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Fare management</Checkbox>)}
                  </FormItem>
                </Col>

                <Col md={12} sm={12} xs={12} style={headingStyle}>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Complaints', {
                        initialValue: Complaints,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Complaints</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Notification', {
                        initialValue: Notification,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Notification</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Rating', {
                        initialValue: Account,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Rating</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Referrals', {
                        initialValue: Referrals,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Referrals</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Reports', {
                        initialValue: Reports,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Reports</Checkbox>)}
                  </FormItem>
                  <FormItem {...tailFormItemLayout} style={{
                      marginBottom: 8
                    }}>
                    {getFieldDecorator('Transaction details', {
                        initialValue: Transaction_details,
                        valuePropName: 'checked'
                       
                      })(<Checkbox>Transaction details</Checkbox>)}
                  </FormItem>
                </Col>

              </Col>

              <FormItem {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                  Edit Admin
                </Button>
              </FormItem>
            </Form>
            <Helmet>
              <style>
                {
                  ` .dYawgl {
                    margin-top: 0 !important;
                  }
                  .ReactTable {
                    margin-top: 20px;
                  }
                  .rt-tr {
                    font-size: 16px;
                  }
                  .rt-tbody {
                    text-align: center;
                  }
                  .rt-tr {
                    cursor: pointer;
                  }
                  .ghxaZn {
                    margin-top: 0 !important;
                  }
                  .isoExampleWrapper {
                    margin-top: 0 !important;
                  }
                  .ant-btn-primary {
                    width: 25% !important;
                    text-align: center !important;
                    float: right !important;
                  }
                  .ant-form-item-label {
                    float: left;
                  }
                   `  }</style>
            </Helmet>

          </Box>
        </Col>
      </Row>
    </LayoutWrapper>);
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
