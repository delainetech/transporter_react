
import clone from 'clone';
import { DateCell, ImageCell, LinkCell, TextCell } from './helperCells';

const renderCell = (object, type, key) => {
  const value = object[key];
  switch (type) {
    case 'ImageCell':
      return ImageCell(value);
    case 'DateCell':
      return DateCell(value);
    case 'LinkCell':
      return LinkCell(value);
    default:
      return TextCell(value);
  }
};

const columns = [
  {
    title: "Id",
    key: 'id',
    width: 100,
    render: object => renderCell(object, 'TextCell', 'id')
  },
  {
    title: "Unique Id",
    key: 'user_id',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'user_id')
  },
  {
    title: "Origin",
    key: 'origin',
    width: 200,
    render: object => renderCell(object, 'TextCell', 'origin')
  },
  {
    title: "Date",
    key: 'origin_date',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'origin_date')
  },{
    title: "Destinations",
    key: 'destination',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'destination')
  },
  {
    title: "Truck Type",
    key: 'truckType',
    width: 300,
    render: object => renderCell(object, 'TextCell', 'truckType')
  },
  {
    title: "Truck Name",
    key: 'truck_name',
    width: 200,
    render: object => renderCell(object, 'DateCell', 'truck_name')
  },
  {
    title:"Truck Number",
    key: 'truck_number',
    width: 100,
    render: object => renderCell(object, 'TestCell', 'truck_number')
  },{
    title: "driver_name",
    key: 'driver_name',
    width: 100,
    render: object => renderCell(object, 'TestCell', 'driver_name')
  },
  {
    title:"Driver Number",
    key: 'driver_number',
    width: 100,
    render: object => renderCell(object, 'TestCell', 'driver_number')
  }
];
const smallColumns = [columns[1], columns[2], columns[3], columns[4], columns[5], columns[6], columns[7], columns[8], columns[9]];



const tableinfos = [
  
  {
    title: 'All Users',
    value: 'filterView',
    columns: clone(smallColumns)
  }
];
export { columns, tableinfos };
