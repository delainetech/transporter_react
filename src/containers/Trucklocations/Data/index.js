import React, { Component } from 'react';
import Tabs, { TabPane } from '../../../components/uielements/tabs';
import LayoutContentWrapper from '../../../components/utility/layoutWrapper';
import TableDemoStyle from './demo.style';
import fakeData from '../truck';
import { tableinfos } from './configs';
import * as TableViews from './tableViews/';
import Notification from '../../../components/notification';
import fetch from 'cross-fetch';


let dataList =null;


export default class AntTable extends Component {


  constructor() {
   super();
   this.state = {
      data:null
   }
 }


componentWillMount() {
 var th = this;
fetch('http://104.131.28.228/transporter/public/api/truckLocationdata')
.then(function(data) {

 var selectedFrequency = JSON.parse(data._bodyText);
 if(selectedFrequency.message==='error')
 {
  Notification(
          'error',
          'error',
          'No Data Found'
        );

 }
 else{
 var mainData = selectedFrequency.data;
var dataLength = mainData.length;

mainData.map(function(mainData1, i){
      
var jsonData = mainData[i].destination;

var myObject = JSON.parse(jsonData);
let decodedData = '';
myObject.map(function(mainData2, j){

   decodedData = decodedData+myObject[j].destination+" : "+myObject[j].amount+"\n"
 return true;
})

mainData[i].destination = decodedData;
return true;
     })

   dataList =new fakeData(dataLength,mainData);
  th.setState({
         data: selectedFrequency.data
     });


}
})
}
  renderTable(tableInfo) {
    let Component;
    switch (tableInfo.value) {
    
      case 'filterView':
        Component = TableViews.FilterView;
        break;
      
      default:
        Component = TableViews.FilterView;
    }
    return <Component tableInfo={tableInfo} dataList={dataList} />;
  }
  render() {

     if (!this.state.data) {
            return <div />
        }

    return (


        <div>
      <LayoutContentWrapper>
        <TableDemoStyle className="isoLayoutContent">
          <Tabs className="isoTableDisplayTab">
            {tableinfos.map(tableInfo => (
              <TabPane tab={tableInfo.title} key={tableInfo.value}>
                {this.renderTable(tableInfo)}
              </TabPane>
            ))}
          </Tabs>
        </TableDemoStyle>
      </LayoutContentWrapper>
      </div>
    );
  }
}
export { TableViews, tableinfos, dataList };
