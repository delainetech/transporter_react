import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';

const InjectMassage1 = props => <FormattedMessage {...props} />;
export default injectIntl(InjectMassage1, {
  withRef: false
});
